var Metalsmith=require('/var/www/html/node_modules/metalsmith');
var markdown=require('/var/www/html/node_modules/metalsmith-markdown');
var layouts=require('/var/www/html/node_modules/metalsmith-layouts');
var permalinks=require('/var/www/html/node_modules/metalsmith-permalinks');
var inPlace = require('/var/www/html/node_modules/metalsmith-in-place')
var fs=require('/var/www/html/node_modules/file-system');
var Handlebars=require('/var/www/html/node_modules/handlebars');
Handlebars.registerPartial('Header_default', fs.readFileSync('/var/www/html/websites/5ae00ccca93fff0013a52971/37736dece785483296a17abe7e3391ee/temp/Header_default.html').toString())
Handlebars.registerPartial('Footer_default', fs.readFileSync('/var/www/html/websites/5ae00ccca93fff0013a52971/37736dece785483296a17abe7e3391ee/temp/Footer_default.html').toString())
 Metalsmith(__dirname)
.metadata({
title: "Demo Title",
description: "Some Description",
generator: "Metalsmith",
url: "http://www.metalsmith.io/"})
.source('/var/www/html/websites/5ae00ccca93fff0013a52971/37736dece785483296a17abe7e3391ee/Preview')
.destination('/var/www/html/websites/5ae00ccca93fff0013a52971/37736dece785483296a17abe7e3391ee/public')
.clean(false)
.use(markdown())
.use(inPlace(true))
.use(layouts({engine:'handlebars',directory:'/var/www/html/websites/5ae00ccca93fff0013a52971/37736dece785483296a17abe7e3391ee/Layout'}))
.build(function(err,files)
{if(err){
console.log(err)
}});